Im Project_0 Pfad liegen einfach zu erkl�rende Datein, die Audiosignale der Line_in Buchse zur Line_out Buchse leiten. 
Die drei Files liegen im src-Pfad.
1. loop leitet einfach die Signale durch den Controller
2. delay leitet nur den rechten Kanal durch den Controller, verz�gert diesen zus�tzlich und gibt das aktuelle und verz�gerte Signal auf beide Ausg�nge der Line_out Buchse aus.
3. echo ist �hnlich delay, f�hrt jedoch einen Teil des verz�gerten Signals zur�ck auf den Eingang und erzeugt so ein mehrfaches Echo.
Alle drei Programme k�nnen nur durch Parameter�nderungen im C-Code ver�ndert werden. Dadurch sind sie einfach zu erkl�ren, jedoch wenig nutzerfreundlich und als Einstieg aber durchaus geeignet. 