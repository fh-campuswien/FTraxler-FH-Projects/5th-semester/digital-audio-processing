// fm4_loop_intr.c

#define DELAY_BUFFER_MAX_SIZE 24000

#include "fm4_wm8731_init.h"
#include "FM4_slider_interface.h"

// Create (instantiate) GUI slider data structure
struct FM4_slider_struct FM4_GUI;
uint32_t delay_buffer[DELAY_BUFFER_MAX_SIZE];
uint16_t buffer_pointer;

void PRGCRC_I2S_IRQHandler(void) 
{
  union WM8731_data sample;
	union WM8731_data delayed_sample;
	float volume, balance, left, right, merged, s2, delay, rLeft, rRight, feedback;
	
  gpio_set(DIAGNOSTIC_PIN,HIGH);
	// Get L/R codec sample
  sample.uint32bit = i2s_rx();
	
	// delay IO
	delayed_sample.uint32bit = delay_buffer[buffer_pointer];
	
	// read slider values
	volume = (FM4_GUI.P_vals[0] / 10);
	balance = (FM4_GUI.P_vals[1] / 10);
	s2 = (FM4_GUI.P_vals[2] / 10);
	delay = (FM4_GUI.P_vals[3] / 10);
	feedback = (FM4_GUI.P_vals[4] / 10);
	
	// process splitted input with volume and balance
	left = (volume * sample.uint16bit[LEFT] * (0.5F - balance));
	right = (volume * sample.uint16bit[RIGHT] * (0.5F + balance));
	
	// middle cancelling
	merged = (left + right) * s2;
	left = left - merged;
	right = right - merged;
	
	// add delay
	left = left + (delayed_sample.uint16bit[LEFT] * delay);
	right = right + (delayed_sample.uint16bit[RIGHT] * delay);
	
	// add reverb
	rLeft = sample.uint16bit[LEFT] + delayed_sample.uint16bit[LEFT] * feedback;
	rRight = sample.uint16bit[LEFT] + delayed_sample.uint16bit[RIGHT] * feedback;
	
	delayed_sample.uint16bit[LEFT] = rLeft;
	delayed_sample.uint16bit[RIGHT] = rRight;
	delay_buffer[buffer_pointer] = delayed_sample.uint32bit;
	buffer_pointer = (buffer_pointer + 1)%DELAY_BUFFER_MAX_SIZE;
	
	// Return L/R samples to codec via C union
	sample.uint16bit[LEFT] = (int16_t) left;
	sample.uint16bit[RIGHT] = (int16_t) right;
  i2s_tx(sample.uint32bit);

  NVIC_ClearPendingIRQ(PRGCRC_I2S_IRQn);
	
	gpio_set(DIAGNOSTIC_PIN,LOW);
}

int main(void)
{
	
	// Initialize the slider interface by setting the baud rate (460800 or 921600)
	// and initial float values for each of the 6 slider parameters

	init_slider_interface(&FM4_GUI,460800, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0);
	
	// Send a string to the PC terminal
	write_uart0("Hello FM4 World!\r\n");
	
// Some #define options for initializing the audio codec interface:
	// FS_8000_HZ, FS_16000_HZ, FS_24000_HZ, FS_32000_HZ, FS_48000_HZ, FS_96000_HZ
	// IO_METHOD_INTR, IO_METHOD_DMA
	// WM8731_MIC_IN, WM8731_MIC_IN_BOOST, WM8731_LINE_IN
  fm4_wm8731_init (FS_48000_HZ,               // Sampling rate (sps)
	                 WM8731_LINE_IN,            // Audio input port
	                 IO_METHOD_INTR,            // Audio samples handler
	                 WM8731_HP_OUT_GAIN_0_DB,   // Output headphone jack Gain (dB)
	                 WM8731_LINE_IN_GAIN_0_DB); // Line-in input gain (dB)
  while(1){
		// Update slider parameters
		update_slider_parameters(&FM4_GUI);	
	}
}
