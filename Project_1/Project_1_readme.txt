Die Project_1 Datein erm�glichen eine Parameter�nderung w�hrend der Laufzeit �ber die Serielle Schnittstelle. Dazu muss das Programm 'Cypress FM4 S6E2CC Parammeter Slider' aufgerufen werden. 
Die notwendigen .dll Files befinden sich im 'FM4_GUI_slider Libs' Ordner, der sich im gleichen Verzeichnis wie die .exe-Datei befinden muss. Eine Installation findet nicht statt. 
Das Programm erkennt das angeschlossene Board und bietet die entsprechende Schnittstelle im COM-Auswahlmenue an. Die serielle Schnittstelle des FM4-Boards ist auszuw�hlen 
und die Baudrate auf 460800 einzustellen (identisch mit der Einstellung im Anwenderprogramm: init_slider_interface(&FM4_GUI,460800, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0); ), 
anschlie�end den 'Connect' Button bet�tigen.
ACHTUNG: 
Entgegen den vermeintlichen Einstellungen unter 'Configuration', erzeugt das Parameter Slider Programm nur vorzeichenbehaftete Ganzzahlen. Die Erzeugung von Kommazahlen muss deshalb im Anwenderprogramm erfolgen. 
Bspl.: 
Eingestellt sind Startwert 0, Endwert 10, Schrittweite 0.1 -> daraus ergibt sich eine Schrittanzahl von 100, weshalb je nach Sliderstellung die Zahlen von 0 bis 100 erzeugt und �bertragen werden.
Eingestellt sind Startwert -0.5, Endwert +0.5, Schrittweite 0.01 -> daraus ergibt sich eine Schrittanzahl von 100, weshalb je nach Sliderstellung die Zahlen von -50 bis +50 erzeugt und �bertragen werden.
Die Anzeige im Sliderprogramm unterhalb der Slider entspricht den gew�nschten, in der Configuration eingestellten Werten. Die Umrechnung der vom Parameter Slider Programm erzeugten Ganzzahlenwerte in Kommawerte ist Aufgabe des Programmierers. 
F�r die Darstellung der Sliderwerte in der Vorlesung ist die Angabe unter den Slidern sicher hilfreich. Es bedarf aber einer Erl�uterung zu den Umrechnungen innerhalb des Anwenderprogramms. Andernfalls k�nnte der Eindruck entstehen, die Sliderwerte werden im Anwenderprogramm nochmals geteilt.

Im Keil-MDK Debugmode kann man im Watch1-Fenster unter FM4-GUI die einzelnen Werte der Slider unter P_val in Echtzeit w�hrend des Programmlaufes (wichtig wegen der Wertaktualisierung) anzeigen lassen. 


0. fm4_loop_intr_GUI.c
Das Programm leitet die Signale der beiden Line_in-Kan�le an die Ausg�nge weiter. Die Verst�rkung/Lautst�rke wird getrennt f�r jeden Kanal mit den Slidern 0 und 1 geregelt. 
Einstellungen f�r Parameter Slider: Links-0-1-10-0.1-100-1.1 / Rechts-0-1-10-0.1-100-1.1

1. fm4_vol_bal_intr_GUI.c
Das Programm leitet die Signale der beiden Line_in-Kan�le an die Ausg�nge weiter. Die Verst�rkung/Lautst�rke wird mit dem Slider 0 f�r beide Kan�le gleich eingestellt. 
Die Balance ist �ber den Slider 1 steuerbar.  
Einstellungen f�r Parameter Slider: Volume-0-1-10-0.1-100-1.1 / Balance--0.5-0-0.5-0.01-100-1.2
 
2. fm4_kara_intr_GUI.c
Das Programm leitet die Signale der beiden Line_in-Kan�le an die Ausg�nge weiter. Die Verst�rkung/Lautst�rke wird mit dem Slider 0 f�r beide Kan�le gleich eingestellt. 
Die Balance ist �ber den Slider 1 steuerbar. Das Herausnehmen der Mitteninformation wird �ber den Slider 2 geregelt.
Einstellungen f�r Parameter Slider: Volume-0-1-10-0.1-100-1.1 / Balance--0.5-0-0.5-0.01-100-1.2 / Kara 0-05-0.05-100-1.1
(empfohlen: True Colors - Cindi Lauper, Sunrise - Nora Jones, Big Yellow Taxi - Counting Crows, tairway to Heaven - Led Zeppelin, Beatles, 

3. fm4_flanger_fix_intr_GUI.c
Das Pogramm erzeugt einen Flanger-Effekt. Dabei wird das Eingangssignal des linken Kanals einmal direkt und zum anderen unterschiedlich lang verz�gert auf beide Ausgangskan�le ausgegeben. 
Die Verz�gerung beruht auf einem Ringspeicher mit zwei Zeigern. An der Stelle des Zeigers i wird kontinuierlich das neue Sample eingetragen. Die Position des Verz�gerungszeiger berechnet 
sich aus einer Verz�gerungszeit, die aus einem Sinussignal generiert wird. 
(empfohlen: Got a brocken Heart - Walter Trout, Shadow on the Wall - Roger Chapman)

4.  fm4_flanger_var_intr_GUI.c
Das Pogramm erzeugt einen Flanger-Effekt. Dabei wird das Eingangssignal des linken Kanals einmal direkt und zum anderen unterschiedlich lang verz�gert auf beide Ausgangskan�le ausgegeben. 
Die Verz�gerung beruht auf einem Ringspeicher mit zwei Zeigern. An der Stelle des Zeigers i wird kontinuierlich das neue Sample eingetragen. Die Position des Verz�gerungszeiger berechnet 
sich aus einer Verz�gerungszeit, die aus einem Sinussignal generiert wird. Die Geschwindigkeit des Flanger-Effektes sowie die Effektlaust�rke lassen sich mit den Slidern variieren.
Einstellungen f�r Parameter Slider: Speed-0,5-1-10-0.1-95-1.1 / Amplitude-0-1-1-0.1-10-1.1
(empfohlen: Got a brocken Heart - Walter Trout, Shadow on the Wall - Roger Chapman)

5.fm4_delay_fix_intr_GUI.c
Das Programm verz�gert die Eingangssignale des linken und rechten Kanals, addiert diese auf die unverz�gerten Signale und gibt diese auf den linken bzw. rechten Kanal aus. 
Die Lautst�rke der Originalsignale k�nnen ebenso wie die Laust�rke der verz�gerten Signale getrennt je Kanal mittels der Slider eingestellt werden. Die Verz�gerungszeit ist fest auf 0,5s eingestellt.  
Einstellungen f�r Parameter Slider: Links-0-1-10-0.1-100-1.1 / Rechts-0-1-10-0.1-100-1.1 / Delay_L-0-1-10-0.1-100-1.1 / Delay_R-0-1-10-0.1-100-1.1 

6.fm4_delay_var_intr_GUI.c
Das Programm verz�gert die Eingangssignale des linken und rechten Kanals, addiert diese auf die unverz�gerten Signale und gibt diese auf den linken bzw. rechten Kanal aus. 
Die Lautst�rke der Originalsignale k�nnen ebenso wie die Laust�rke der verz�gerten Signale getrennt je Kanal mittels der Slider eingestellt werden. 
Die Verz�gerungszeit ist variabel mittel Slider[4] zwischen 0,5 und 24 (entspricht 1ms bis 0,5s) einstellbar.   
Einstellungen f�r Parameter Slider: Links-0-1-10-0.1-100-1.1 / Rechts-0-1-10-0.1-100-1.1 / Delay_L-0-1-10-0.1-100-1.1 / Delay_R-0-1-10-0.1-100-1.1 / Zeit-0.5-1-24-0.1-235-1.1

7.fm4_left_right_var_intr_GUI.c
Das Programm erm�glicht die Positionierung der Tonquelle zwischen dem linken und rechten Kanal. Erm�glicht wird dies durch eine Balanceregelung der beiden Originaleingangssignale, 
jedoch mit zus�tzlicher Mischung der verz�gerten Eingangssignale, wodurch der Sound deutlich an Raumf�lle gewinnt. Mit dem Slider[0] kann die Lautst�rke des unverz�gerten Signals 
eingestellt werden, mit dem Slider[1] wird die r�umliche Position gew�hlt. (Slider[2] ist ohne Funktion. Slider[3] regelt die Lautst�rke der beiden verz�gerten Signale und mittels 
Slider[4] ist die Verz�gerungszeit zwischen 0,5 und 24 (entspricht 1ms bis 0,5s) einstellbar. 
Einstellungen f�r Parameter Slider: Volume-0-1-10-0.1-100-1.1 / Balance-0-5-10-0.1-100-1.1 / VolumeDelay-0-1-10-0.1-100-1.1 / P3-0-1-10-0.1-100-1.1 / Zeit-0.5-1-24-0.1-235-1.1

8.fm4_echo_var_intr_GUI.c
Das Programm erzeugt ein mehrfaches Echo durch R�ckkopplung verz�gerter Eingangsdaten. Slider[0] steuert die Lautst�rke des unverz�gerten Originalsignals.  
Slider[1] steuert die Lautst�rke des verz�gerten und mehrfach r�ckgekoppelten Signals.  Mit dem Slider[2] wird die R�ckkopplungsamplitude eingestellt, die niemals 1 werden sollte. 
 Slider[3] steuert die Lautst�rke am Eingang der Verz�gerungsstrecke, der neben Slider[1] damit auch unmittelbaren Einfluss auf die Ausgabelautst�rke des Verz�gerten Signals besitzt. 
 Slider[4] schlie�lich regelt die Gr��e des Verz�gerungsspeichers. 
 Einstellungen f�r Parameter Slider: Volume-0-1-10-0.1-100-1.1 / Volume_Echo-0-1-10-0.1-100-1.1 / Volume_Feedbck-0-1-10-0.1-100-1.1 / Volume_Input_Delay-0-1-10-0.1-100-1.1 / Zeit-0.5-1-24-0.1-235-1.1