/*******************************************************************************
* Copyright (C) 2014 Spansion LLC. All Rights Reserved.
*
* This software is owned and published by:
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software contains source code for use with Spansion
* components. This software is licensed by Spansion to be adapted only
* for use in systems utilizing Spansion components. Spansion shall not be
* responsible for misuse or illegal use of this software for devices not
* supported herein.  Spansion is providing this software "AS IS" and will
* not be responsible for issues arising from incorrect user implementation
* of the software.
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
* REGARDING THE SOFTWARE (INCLUDING ANY ACOOMPANYING WRITTEN MATERIALS),
* ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED USE, INCLUDING,
* WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, THE IMPLIED
* WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR USE, AND THE IMPLIED
* WARRANTY OF NONINFRINGEMENT.
* SPANSION SHALL HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT,
* NEGLIGENCE OR OTHERWISE) FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT
* LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION,
* LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS) ARISING FROM USE OR
* INABILITY TO USE THE SOFTWARE, INCLUDING, WITHOUT LIMITATION, ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA,
* SAVINGS OR PROFITS,
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* YOU ASSUME ALL RESPONSIBILITIES FOR SELECTION OF THE SOFTWARE TO ACHIEVE YOUR
* INTENDED RESULTS, AND FOR THE INSTALLATION OF, USE OF, AND RESULTS OBTAINED
* FROM, THE SOFTWARE.
*
* This software may be replicated in part or whole for the licensed use,
* with the restriction that this Disclaimer and Copyright notice must be
* included with each copy of this software, whether used in part or whole,
* at all times.
*/
/******************************************************************************/
/** \file main.c
 **
 ** Main Module
 **
 ** \brief  This example write 256 words to the PSRAM (SV6P161UFC) provided on
 **         the SK-FM4-176L-S6E2CC board and checks it by reading them out
 **         again. Fail is display by yellow LED color and green for success.
 **         Red color shows unsuccessful initialization of the external bus
 **         interface.
 **
 ** History:
 **   - 2014-12-10  1.0  MWi        First release version.
 **
 ******************************************************************************/

/******************************************************************************/
/* Include files                                                              */
/******************************************************************************/
#include "pdl.h"

/******************************************************************************/
/* Local pre-processor symbols/macros ('#define')                             */
/******************************************************************************/
#define PSRAM_BASE_ADDRESS  (0x60000000ul)

#define BOARD_LED_R         bFM4_GPIO_PDOR1_PA
#define BOARD_LED_G         bFM4_GPIO_PDORB_P2
#define BOARD_LED_B         bFM4_GPIO_PDOR1_P8

#define BOARD_LED_ON        (0u)
#define BOARD_LED_OFF       (1u)

/**
 ******************************************************************************
 ** \brief  GPIO init function for RGB LED and the External Bus Interface
 **
 ******************************************************************************/
static void Init_Gpio(void)
{
  Gpio1pin_InitOut(bFM4_GPIO_PDOR1_PA, Gpio1pin_InitDirectionOutput); // LED RED
  Gpio1pin_InitOut(bFM4_GPIO_PDORB_P2, Gpio1pin_InitDirectionOutput); // LED GREEN
  Gpio1pin_InitOut(GPIO1PIN_P18, Gpio1pin_InitDirectionOutput); // LED BLUE
  
  BOARD_LED_R = BOARD_LED_OFF;     // LED RED
  BOARD_LED_G = BOARD_LED_OFF;     // LED GREEN
  BOARD_LED_B = BOARD_LED_OFF;     // LED BLUE
  
  // I/O setup for PSRAM with A[19:1], D[15:0], CSX, OEX, WEX, UBX, and LBX
  SetPinFunc_MADATA00_0();
  SetPinFunc_MADATA01_0();
  SetPinFunc_MADATA02_0();
  SetPinFunc_MADATA03_0();
  SetPinFunc_MADATA04_0();
  SetPinFunc_MADATA05_0();
  SetPinFunc_MADATA06_0();
  SetPinFunc_MADATA07_0();
  SetPinFunc_MADATA08_0();
  SetPinFunc_MADATA09_0();
  SetPinFunc_MADATA10_0();
  SetPinFunc_MADATA11_0();
  SetPinFunc_MADATA12_0();
  SetPinFunc_MADATA13_0();
  SetPinFunc_MADATA14_0();
  SetPinFunc_MADATA15_0();
  SetPinFunc_MAD01_0();
  SetPinFunc_MAD02_0();
  SetPinFunc_MAD03_0();
  SetPinFunc_MAD04_0();
  SetPinFunc_MAD05_0();
  SetPinFunc_MAD06_0();
  SetPinFunc_MAD07_0();
  SetPinFunc_MAD08_0();
  SetPinFunc_MAD09_0();
  SetPinFunc_MAD10_0();
  SetPinFunc_MAD11_0();
  SetPinFunc_MAD12_0();
  SetPinFunc_MAD13_0();
  SetPinFunc_MAD14_0();
  SetPinFunc_MAD15_0();
  SetPinFunc_MAD16_0();
  SetPinFunc_MAD17_0();
  SetPinFunc_MAD18_0();
  SetPinFunc_MAD19_0();
  SetPinFunc_MAD20_0();
  SetPinFunc_MOEX_0();
  SetPinFunc_MWEX_0();
  SetPinFunc_MCSX0_0();
  SetPinFunc_MDQM0_0();
  SetPinFunc_MDQM1_0();
}

/**
 ******************************************************************************
 ** \brief  EBI PSRAM init function
 **
 ** \retval  Ok                      EXTIF sucessfully initialized with PSRAM
 **                                  parameters
 ** \retval  ErrorInvalidParameter   One or more parameters illegal
 ******************************************************************************/
static en_result_t Init_PSram(void)
{
  stc_extif_area_config_t stcExtIF;        // Configuration for external bus
  
  PDL_ZERO_STRUCT(stcExtIF);               // Clear out EXTIF structure
  
  // Reset External Bus for clean start.
  Clk_PeripheralClockDisable(ClkGateExtif);
  Clk_PeripheralSetReset(ClkResetExtif);
  Clk_PeripheralClearReset(ClkResetExtif);
  Clk_PeripheralClockEnable(ClkGateExtif);

  stcExtIF.enWidth                    = Extif16Bit;   ///< 8, 16 bit data bus width. See description of #en_extif_width_t
  stcExtIF.bReadByteMask              = TRUE;         ///< TRUE: Read Byte Mask enable
  stcExtIF.bWriteEnableOff            = FALSE;        ///< TRUE: Write enable disabled
  stcExtIF.bNandFlash                 = FALSE;        ///< TRUE: NAND Flash bus enable, FLASE: NOR Flash/SRAM bus enable
  stcExtIF.bPageAccess                = FALSE;        ///< TRUE: NOR Flash memory page access mode enabled
  stcExtIF.bRdyOn                     = FALSE;        ///< TRUE: RDY mode enabled
  stcExtIF.bStopDataOutAtFirstIdle    = TRUE;         ///< TRUE: Stop to write data output at first idle cycle, FALSE: Extends to write data output to the last idle cycle
  stcExtIF.bMultiplexMode             = FALSE;        ///< TRUE: Multiplex mode
  stcExtIF.bAleInvert                 = FALSE;        ///< TRUE: Invert ALE signal (negative polarity)
  stcExtIF.bAddrOnDataLinesOff        = TRUE;         ///< TRUE: Do not output address to data lines (Hi-Z during ALC cycle period)
  stcExtIF.bMpxcsOff                  = FALSE;        ///< TRUE: Do not assert MCSX in ALC cycle period
  stcExtIF.bMoexWidthAsFradc          = FALSE;        ///< TRUE: MOEX width is set with FRADC, FALSE: MOEX width is set with RACC-RADC
  stcExtIF.bMclkoutEnable             = TRUE;         ///< TRUE: Enables MCLKOUT pin
  stcExtIF.u8MclkDivision             = 3u;           ///< Division ratio for MCLK (1 ... 16 div)
  stcExtIF.bPrecedReadContinuousWrite = FALSE;        ///< TRUE: Enables preceding read and continuous write request
  stcExtIF.enReadAccessCycle          = Extif4Cycle;  ///< Read Access Cycle timing
  stcExtIF.enReadAddressSetupCycle    = Extif0Cycle;  ///< Read Address Set-up Cycle timing
  stcExtIF.enFirstReadAddressCycle    = Extif1Cycle;  ///< First Read Address Cycle timing
  stcExtIF.enReadIdleCycle            = Extif1Cycle;  ///< Read Idle Cycle timing
  stcExtIF.enWriteAccessCycle         = Extif5Cycle;  ///< Write Access Cycle timing
  stcExtIF.enWriteAddressSetupCycle   = Extif1Cycle;  ///< Write Address Set-up Cycle timing
  stcExtIF.enWriteEnableCycle         = Extif4Cycle;  ///< Write Enable Cycle timing
  stcExtIF.enWriteIdleCycle           = Extif1Cycle;  ///< Write Idle Cycle timing
  stcExtIF.u8AreaAddress              = (uint8_t)(PSRAM_BASE_ADDRESS >> 20ul); ///< Address bits [27:20]
  stcExtIF.enAreaMask                 = Extif2MB;     ///< See description of #en_extif_mask_t
  stcExtIF.enAddressLatchCycle        = Extif1Cycle;  ///< Address Latch Cycles
  stcExtIF.enAddressLatchWidthCycle   = Extif1Cycle;
  
  return (Extif_InitArea(0u, &stcExtIF));
}

/**
 ******************************************************************************
 ** \brief  Main function of PDL
 **
 ** \return int32_t return value, if needed
 ******************************************************************************/
int32_t main(void)
{
  uint16_t au16PsRamData[256u];
  uint16_t u16Counter;
  
  Init_Gpio();
    
  if (Ok != Init_PSram())       // Init of EXTIF with PSRAM parameter successful?
  {
    BOARD_LED_R = BOARD_LED_ON; // Red LED: EXTIF init error
    
    while(1u)
    {}
  }

  // Make some pseudo random data
  for (u16Counter = 0u; u16Counter < 256u; u16Counter++)
  {
    au16PsRamData[u16Counter] = 0xAAAAu ^ (u16Counter * 13u) ^ ((u16Counter + 7u) << 9u);
  }

  // Write data to PSRAM
  for (u16Counter = 0u; u16Counter < 256u; u16Counter++)
  {
    *((uint16_t*)(PSRAM_BASE_ADDRESS + 2u * u16Counter)) = au16PsRamData[u16Counter];
  }
  
  // Read back data from PSRAM and check with written data
  for (u16Counter = 0u; u16Counter < 256u; u16Counter++)
  {
    if (*(uint16_t*)(PSRAM_BASE_ADDRESS + 2u * u16Counter) != au16PsRamData[u16Counter])
    {
      // Yellow LED: EXTIF data write/read failed
      BOARD_LED_R = BOARD_LED_ON;
      BOARD_LED_G = BOARD_LED_ON;
      
      while(1u)
      {}
    }
  }
  
  BOARD_LED_G = BOARD_LED_ON; // Green LED: Everything fine ...
      
  while(1u)
  {}
}

/******************************************************************************/
/* EOF (not truncated)                                                        */
/******************************************************************************/
