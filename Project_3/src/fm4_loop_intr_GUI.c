// fm4_loop_intr.c

#include "fm4_wm8731_init.h"
#include "FM4_slider_interface.h"

// Create (instantiate) GUI slider data structure
struct FM4_slider_struct FM4_GUI;

void PRGCRC_I2S_IRQHandler(void) 
{
  union WM8731_data sample;
	int16_t xL, xR;
	
  gpio_set(DIAGNOSTIC_PIN,HIGH);
	// Get L/R codec sample
  sample.uint32bit = i2s_rx();
	
	// Breakout and then process L and R samples with
	// slider parameters for gain control
	xL = (int16_t) (FM4_GUI.P_vals[0] * sample.uint16bit[LEFT]);
	xR = (int16_t) (FM4_GUI.P_vals[1] * sample.uint16bit[RIGHT]);
	// Do more processing on xL and xR
	// TBD

	
	// Return L/R samples to codec via C union
	sample.uint16bit[LEFT] = xL;
	sample.uint16bit[RIGHT] = xR;
  i2s_tx(sample.uint32bit);

  NVIC_ClearPendingIRQ(PRGCRC_I2S_IRQn);
	
	gpio_set(DIAGNOSTIC_PIN,LOW);
}

int main(void)
{
	
	// Initialize the slider interface by setting the baud rate (460800 or 921600)
	// and initial float values for each of the 6 slider parameters

	init_slider_interface(&FM4_GUI,460800, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0);
	
	// Send a string to the PC terminal
	write_uart0("Hello FM4 World!\r\n");
	
// Some #define options for initializing the audio codec interface:
	// FS_8000_HZ, FS_16000_HZ, FS_24000_HZ, FS_32000_HZ, FS_48000_HZ, FS_96000_HZ
	// IO_METHOD_INTR, IO_METHOD_DMA
	// WM8731_MIC_IN, WM8731_MIC_IN_BOOST, WM8731_LINE_IN
  fm4_wm8731_init (FS_48000_HZ,               // Sampling rate (sps)
	                 WM8731_LINE_IN,            // Audio input port
	                 IO_METHOD_INTR,            // Audio samples handler
	                 WM8731_HP_OUT_GAIN_0_DB,   // Output headphone jack Gain (dB)
	                 WM8731_LINE_IN_GAIN_0_DB); // Line-in input gain (dB)
  while(1){
		// Update slider parameters
		update_slider_parameters(&FM4_GUI);	
	}
}
