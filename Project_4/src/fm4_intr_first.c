// fm4_loop_intr_first.c

#include "fm4_wm8731_init.h"

#define N_period 48
// 48 sample cosine lookup table with Apeak = 15000
int16_t onek_table[48] = {15000,  14871,  14488,  13858,  12990,  
                          11900,  10606,   9131,   7500,   5740,
                           3882,   1957,      0,  -1957,  -3882,  
                          -5740,  -7499,  -9131, -10606, -11900, 
                         -12990, -13858, -14488, -14871, -15000,
                         -14871, -14488, -13858, -12990, -11900, 
                         -10606,  -9131,  -7500,  -5740,  -3882,
                          -1957,      0,   1957,   3882,   5740,
                           7500,   9131,  10606,  11900,  12990,
                          13858,  14488,  14871};
int16_t table_indexL = 0;
int16_t table_indexR = 0;

void PRGCRC_I2S_IRQHandler(void) 
{
  union WM8731_data sample;

  gpio_set(DIAGNOSTIC_PIN,HIGH);
	//gpio_toggle(DIAGNOSTIC_PIN);
  
	// Load the two channel ADC sample into the union sample
	sample.uint32bit = i2s_rx();
	
	// Put table values in left and right channels 
	sample.uint16bit[LEFT] = onek_table[table_indexL];
	sample.uint16bit[RIGHT] = onek_table[table_indexR];
	table_indexL = (table_indexL + 1) % N_period; // stride by 1 <=> 1 kHz
	table_indexR = (table_indexR + 2) % N_period; // stride by 2 <=> 2 kHz
	
	// Pass or push sample back out to the DAC
  i2s_tx(sample.uint32bit);

  NVIC_ClearPendingIRQ(PRGCRC_I2S_IRQn);
	gpio_set(DIAGNOSTIC_PIN,LOW);
}

int main(void)
{
	// Some #define options for initializing the audio codec interface:
	// FS_8000_HZ, FS_16000_HZ, FS_24000_HZ, FS_32000_HZ, FS_48000_HZ, FS_96000_HZ
	// IO_METHOD_INTR, IO_METHOD_DMA
	// WM8731_MIC_IN, WM8731_MIC_IN_BOOST, WM8731_LINE_IN
  fm4_wm8731_init (FS_48000_HZ,               // Sampling rate (sps)
	                 WM8731_LINE_IN,            // Audio input port
	                 IO_METHOD_INTR,            // Audio samples handler
	                 WM8731_HP_OUT_GAIN_0_DB,   // Output headphone jack Gain (dB)
	                 WM8731_LINE_IN_GAIN_0_DB); // Line-in input gain (dB)
  while(1){}
}
